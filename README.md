# Liferay with Nginx Docker Demo

This runs a Liferay 7.1 CE instance behind an Nginx with a Domain - it is used for demo purposes only and has no real data persistence.

## Prerequisites

* Installed Docker
* Installed docker-compose
* A brief understanding of Docker, Liferay and the Terminal of your operating system

### Installing the demo

Clone the project from [Bitbucket](https://bitbucket.org/liferaysamples/liferay-nginx-docker-demo/) and you are ready to go.

## Getting started

Add an entry in your `/etc/hosts`

    127.0.0.1   localliferay.at

Start the container with 

    # sudo docker-compose up

Goto `http://localliferay.at/`

Optional: Set the virtual host for a site to localliferay.at

Optional: Comment the marked line in the `docker-compose.yml` so Liferay is accessible via the Nginx only.

## Conclusion

Its very easy to set an Nginx reverse proxy upfront a Liferay container.

## Authors

* **Manuel Manhart** - *Initial work*

## Built With

* [Docker](http://docs.docker.com/) - The container base
* [Docker-Compose](https://docs.docker.com/compose/) - Composing multiple containers into one service
* [VS Code](https://code.visualstudio.com/) - Used to edit all the files
* [Liferay Portal](https://www.liferay.com/) - The service used in the container

## see also

[Liferay at Docker Hub](https://hub.docker.com/r/liferay/portal)

## Contributing

To the time of writing this is a finished demo project, so contributing is not planned.

## Versioning

We do not use versioning since this is a project that was built solely for demo purposes.

## License

This project is licensed under MIT license, feel free to clone and modify as you wish.

## Open issues

none I know of, if you find any, feel free to contact me
